//
//  NewHomeViewController.h
//  Flamer Pro
//
//  Created by zinzu on 27/10/2016.
//  Copyright © 2016 AppDupe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FBSessionTokenCachingStrategy.h>
#import <FacebookSDK/FacebookSDK.h>
#import "TinderFBFQL.h"
#import "EGOImageView.h"
#import <QuartzCore/QuartzCore.h>
#import "RoundedImageView.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ChatViewController.h"
#import "UIImageView+Download.h"
#import "QuestionVC.h"
#import "ProfileVC.h"
#import "MomentsVC.h"
#import "MenuViewController.h"

@class MenuViewController,MomentsVC,ChatViewController;
@interface NewHomeViewController : BaseVC< TinderFBFQLDelegate,UIScrollViewDelegate>
{
    IBOutlet UILabel *lblNoFriendAround;
    IBOutlet UIView *homeScreenView;
    IBOutlet UIButton *btnInvite;
}
@property (weak, nonatomic) IBOutlet UIView *navBar;
- (IBAction)openMail:(id)sender;


@end
