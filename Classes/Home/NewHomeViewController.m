//
//  NewHomeViewController.m
//  Flamer Pro
//
//  Created by zinzu on 27/10/2016.
//  Copyright © 2016 AppDupe. All rights reserved.
//

#import "NewHomeViewController.h"
#import "JSDemoViewController.h"

@interface NewHomeViewController ()
{
    IBOutlet EGOImageView *profileImageView;
    BOOL inAnimation;
    CALayer *waveLayer;
    NSMutableArray *myProfileMatches;
}

@end

@implementation NewHomeViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getLocation];
    [self initUI];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(userDidLoginToXmpp) name:NOTIFICATION_XMPP_LOGGED_IN object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(homeScreenRefresh) name:NOTIFICATION_HOMESCREEN_REFRESH object:nil];
    
   // [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(navigateToNewScreenWithInfo:) name:NOTIFICATION_SCREEN_NAVIGATION_BUTTON_CLICKED object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initUI {
    CALayer *bottomBorder = [CALayer layer];
    
    bottomBorder.frame = CGRectMake(0.0f, 43.0f, _navBar.frame.size.width, 1.0f);
    _navBar.backgroundColor = [UIColor colorWithRed:248 green:248 blue:248 alpha:1.0f];
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f
                                                     alpha:1.0f].CGColor;
    
    [_navBar.layer addSublayer:bottomBorder];
    
    lblNoFriendAround.hidden = NO;
    btnInvite.hidden = YES;
    
    [Helper setToLabel:lblNoFriendAround Text:@"Finding People around you." WithFont:SEGOUE_UI FSize:17 Color:[UIColor blackColor]];
    lblNoFriendAround.textAlignment = NSTextAlignmentCenter;

    profileImageView.contentMode = UIViewContentModeScaleAspectFill;
    profileImageView.clipsToBounds = YES;
    [profileImageView.layer setCornerRadius:55.0];
    [profileImageView.layer setMasksToBounds:YES];
    
   // [homeScreenView addSubview:profileImageView];
    
    //viewItsMatched.backgroundColor = [Helper getColorFromHexString:@"#000000" :1.0];
    inAnimation = NO;
    waveLayer=[CALayer layer];
    waveLayer.frame = CGRectMake(profileImageView.frame.origin.x + profileImageView.frame.size.width/2-6
                                 , profileImageView.frame.origin.y + profileImageView.frame.size.height/2-9, 10, 10);
    waveLayer.borderWidth =0.2;
    waveLayer.cornerRadius =5.0;
    [homeScreenView.layer addSublayer:waveLayer];
    profileImageView.hidden = NO;
    [waveLayer setHidden:NO];
    
    [homeScreenView bringSubviewToFront:profileImageView];
    }

-(void)getLocation
{
    [[LocationHelper sharedObject]startLocationUpdatingWithBlock:^(CLLocation *newLocation, CLLocation *oldLocation, NSError *error) {
        if (!error) {
            [[LocationHelper sharedObject]stopLocationUpdating];
            [super updateLocation];
        }
    }];
    
}

-(void)homeScreenRefresh
{
    if ([User currentUser].profile_pic!=nil)
    {
        [profileImageView setShowActivity:YES];
        [profileImageView setImageURL:[NSURL URLWithString:[User currentUser].profile_pic]];
    }
    
    //if ([[[XmppCommunicationHandler sharedInstance]xmppStream] isConnected])
   // {
        [self performSelector:@selector(sendRequestForGetMatches) withObject:nil afterDelay:1];
   // }
    [self performSelector:@selector(startAnimation) withObject:nil];
}


//Notification Handler
-(void)userDidLoginToXmpp
{
    [self sendRequestForGetMatches];
}

-(void)sendRequestForGetMatches
{
    NSMutableDictionary *paramDict = [[NSMutableDictionary alloc] init];
    [paramDict setObject:[User currentUser].fbid forKey:PARAM_ENT_USER_FBID];
    
    AFNHelper *afn=[[AFNHelper alloc]init];
    [afn getDataFromPath:METHOD_FINDMATCHES withParamData:paramDict withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             if ([[response objectForKey:@"errFlag"] intValue]==0)
             {
                 NSArray *matches = response[@"matches"];
                 if ([matches count] > 0) {
                     [self performSelectorOnMainThread:@selector(fetchMatchesData:) withObject:matches waitUntilDone:NO];
                 }else{
                     [Helper setToLabel:lblNoFriendAround Text:@"There's no one new around you." WithFont:SEGOUE_UI FSize:17 Color:[UIColor blackColor]];
                     btnInvite.hidden = NO;
                     lblNoFriendAround = NO;
                    [waveLayer setHidden:YES];
                 }
             }
             else{
                 [Helper setToLabel:lblNoFriendAround Text:@"There's no one new around you." WithFont:SEGOUE_UI FSize:17 Color:[UIColor blackColor]];
                 btnInvite.hidden = NO;
                 lblNoFriendAround = NO;
                 [waveLayer setHidden:YES];
             }
         }else{
             [Helper setToLabel:lblNoFriendAround Text:@"There's no one new around you." WithFont:SEGOUE_UI FSize:17 Color:[UIColor blackColor]];
             btnInvite.hidden = NO;
             lblNoFriendAround = NO;
             [waveLayer setHidden:YES];
         }
     }];
}
-(void)startAnimation
{
    if ([waveLayer isHidden] || ![homeScreenView window] || inAnimation == YES)
    {
        return;
    }
    inAnimation = YES;
    [self waveAnimation:waveLayer];
}

-(void)waveAnimation:(CALayer*)aLayer
{
    CABasicAnimation *transformAnimation = [CABasicAnimation animationWithKeyPath:@"transform"];
    transformAnimation.duration = 3;
    transformAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transformAnimation.removedOnCompletion = YES;
    transformAnimation.fillMode = kCAFillModeRemoved;
    [aLayer setTransform:CATransform3DMakeScale( 10, 10, 1.0)];
    [transformAnimation setDelegate:self];
    
    CATransform3D xform = CATransform3DIdentity;
    // xform = CATransform3DScale(xform, 40, 40, 1.0);
    xform = CATransform3DScale(xform, 32, 32, 1.0);
    transformAnimation.toValue = [NSValue valueWithCATransform3D:xform];
    [aLayer addAnimation:transformAnimation forKey:@"transformAnimation"];
    
    
    UIColor *fromColor = [UIColor colorWithRed:255 green:120 blue:0 alpha:1];
    UIColor *toColor = [UIColor colorWithRed:255 green:120 blue:0 alpha:0.1];
    CABasicAnimation *colorAnimation = [CABasicAnimation animationWithKeyPath:@"backgroundColor"];
    colorAnimation.duration = 3;
    colorAnimation.fromValue = (id)fromColor.CGColor;
    colorAnimation.toValue = (id)toColor.CGColor;
    
    [aLayer addAnimation:colorAnimation forKey:@"colorAnimationBG"];
    
    
    UIColor *fromColor1 = [UIColor colorWithRed:0 green:255 blue:0 alpha:1];
    UIColor *toColor1 = [UIColor colorWithRed:0 green:255 blue:0 alpha:0.1];
    CABasicAnimation *colorAnimation1 = [CABasicAnimation animationWithKeyPath:@"borderColor"];
    colorAnimation1.duration = 3;
    colorAnimation1.fromValue = (id)fromColor1.CGColor;
    colorAnimation1.toValue = (id)toColor1.CGColor;
    
    [aLayer addAnimation:colorAnimation1 forKey:@"colorAnimation"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    inAnimation = NO;
    [self performSelectorInBackground:@selector(startAnimation) withObject:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}

-(void)setupMatchesView
{
   // self.decision.hidden = YES;
    
    if ([myProfileMatches count] > 0)
    {
        lblNoFriendAround.hidden = YES;
       // NSDictionary *match = [myProfileMatches objectAtIndex:0];
        //mainImageView.contentMode = UIViewContentModeScaleAspectFill;
        //mainImageView.clipsToBounds = YES;
        
        //[mainImageView setShowActivity:YES];
       // [mainImageView setImageURL:[NSURL URLWithString:[match valueForKey:@"pPic"]]];
       // [mainImageView setBackgroundColor:[UIColor whiteColor]];
       // [mainImageView setPlaceholderImage:[UIImage imageNamed:@"pfImage.png"]];
        
        
       
        
        [waveLayer setHidden:YES];
        [profileImageView setHidden:YES];
        [btnInvite setHidden:YES];
        [lblNoFriendAround setHidden:YES];
        
        
       // [matchesView setHidden:NO];
       // [btnInvite setHidden:YES];
        
        
       // originalPositionOfvw1 = visibleView1.center;
       // visibleView1.hidden = NO;
        
        
       
     }
    else
    {
       
        [waveLayer setHidden:NO];
        [profileImageView setHidden:NO];
        // [self performSelector:@selector(startAnimation) withObject:nil];
    }
}
-(IBAction)openMail :(id)sender
{
    [super sendMailSubject:@"Flamer App!" toRecipents:[NSArray arrayWithObject:@""] withMessage:@"I am using Flamer App ! Whay don't you try it out…<br/>Install Flamer now !<br/><b>Google Play :-</b> <a href='https://play.google.com/store/apps/details?id=com.appdupe.flamernofb'>https://play.google.com/store/apps/details?id=com.appdupe.flamernofb</a><br/><b>iTunes :-</b>"];
}

@end
